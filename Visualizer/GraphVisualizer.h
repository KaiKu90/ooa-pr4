//
// Created by Kai Kuhlmann on 23.05.16.
//

#ifndef OOA_PR4_GRAPHVISUALIZER_H
#define OOA_PR4_GRAPHVISUALIZER_H

#include <cxcore.h>
#include "../Graph/DiGraph.h"

class GraphVisualizer {

public:
    virtual void render(DiGraph &g) = 0;
    virtual void show() = 0;
    virtual void highlightPath(vector<Edge*> path) = 0;
};

#endif //OOA_PR4_GRAPHVISUALIZER_H
