//
// Created by Kai Kuhlmann on 25.05.16.
//

#ifndef OOA_PR4_BIKE_H
#define OOA_PR4_BIKE_H

#include "Vehicle.h"

class Bicycle : public Vehicle {

private:
    float speed;

public:
    Bicycle(int speed) : Vehicle(speed) {
        this->speed = speed;
    };

    float neededTimeForDistance(float dist) override {
        return (dist / speed) * 60;
    }
};

#endif //OOA_PR4_BIKE_H
