//
// Created by Kai Kuhlmann on 25.05.16.
//

#ifndef OOA_PR4_CAR_H
#define OOA_PR4_CAR_H

#include "Vehicle.h"

class Car : public Vehicle {

private:
    float speed;

public:
    Car(int speed) : Vehicle(speed) {
        this->speed = speed;
    };

    float neededTimeForDistance(float dist) override {
        return (dist / speed) * 60;
    }
};

#endif //OOA_PR4_CAR_H
