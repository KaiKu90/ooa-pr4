//
// Created by Kai Kuhlmann on 25.05.16.
//

#ifndef OOA_PR4_VEHICLE_H
#define OOA_PR4_VEHICLE_H

class Vehicle {

protected:
    float speed;

public:
    Vehicle(int speed);
    virtual float neededTimeForDistance(float dist) = 0;
};

Vehicle::Vehicle(int speed) {
    this->speed = speed;
}

#endif //OOA_PR4_VEHICLE_H
