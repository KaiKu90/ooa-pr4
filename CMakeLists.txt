cmake_minimum_required(VERSION 3.5)
project(OOA_PR4)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

find_package(OpenCV REQUIRED)
include_directories(${OpenCV_INCLUDE_DIR})

set(SOURCE_FILES main.cpp Graph/Node.h Graph/Edge.h Graph/DiGraph.h Visualizer/OpenCVGraphVisualizer.h Visualizer/GraphVisualizer.h Graph/Navigator.h Vehicle/Vehicle.h Vehicle/Car.h Vehicle/Bicycle.h Vehicle/Motorcycle.h)
add_executable(OOA_PR4 ${SOURCE_FILES})

target_link_libraries(OOA_PR4 ${OpenCV_LIBS})