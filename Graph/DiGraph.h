//
// Created by Kai Kuhlmann on 04.05.16.
//

#ifndef OOA_PR4_DIGRAPH_H
#define OOA_PR4_DIGRAPH_H


#include <string>
#include <set>
#include <queue>
#include <cmath>
#include <map>
#include "Node.h"
#include "Edge.h"

using namespace std;

class GraphVisualizer;

class DiGraph {

private:
    vector<Node*> nodes;
    GraphVisualizer* graphviz;

public:
    DiGraph();
    ~DiGraph();

    void addNode(Node *node);
    void addEdge(string key1, string key2, float weight);

    vector<Node*> getNeighbours(string key);
    vector<Edge*> getEdges(string key);
    vector<Node*> getNodes();
    Node* getNode(string key);

    vector<Edge*> dijkstraShortestPath(Node *start, Node *end);
    void setVisualizer(GraphVisualizer* graphviz);
    GraphVisualizer* getVisualizer();
};

DiGraph::DiGraph() {}
DiGraph::~DiGraph() {}

void DiGraph::addNode(Node *node) {
    nodes.push_back(node);
}

void DiGraph::addEdge(string key1, string key2, float weight) {
    Edge *edge = new Edge();

    for (int i = 0; i < nodes.size(); i++) {
        if (nodes[i]->getKey() == key1) {
            edge->setStartNode(nodes[i]);

            for (int j = 0; j < nodes.size(); j++) {
                if (nodes[j]->getKey() == key2) {
                    edge->setEndNode(nodes[j]);
                    edge->setWeight(weight);

                    nodes[i]->setNewEdge(edge);
                    break;
                }
            }
        }
    }
}

vector<Node*> DiGraph::getNeighbours(string key) {
    vector<Node*> nbs;
    vector<Edge*> node_edges = getEdges(key);

    for (int i = 0; i < node_edges.size(); i++) {
        nbs.push_back(node_edges[i]->getEndNode());
    }

    return nbs;
}

vector<Edge*> DiGraph::getEdges(string key) {
    vector<Edge*> edges;

    for (int i = 0; i < nodes.size(); i++) {
        if (nodes[i]->getKey() == key) {
            edges = nodes[i]->getEdges();
        }
    }

    return edges;
}

vector<Node*> DiGraph::getNodes() {
    return nodes;
}

Node* DiGraph::getNode(string key) {
    for (int i = 0; i < nodes.size(); i++) {
        if (nodes[i]->getKey() == key) return nodes[i];
    }

    return nullptr;
}

vector<Edge*> DiGraph::dijkstraShortestPath(Node *start, Node *end) {
    vector<Edge*> result;
    set<pair<Node*, float>> q;
    map<Node*, float> dist;
    map<Node*, Node*> prev;

    for (int i = 0; i < nodes.size(); i++) {
        if (nodes[i] == start) dist[start] = 0;
        else dist[nodes[i]] = numeric_limits<float>::infinity();

        q.insert(make_pair(nodes[i], dist[nodes[i]]));
    }

    while (!q.empty()) {
        Node *u = q.begin()->first;
        q.erase(q.begin());
        vector<Edge*> outEdges = u->getEdges();

        for (int i = 0; i < outEdges.size(); i++) {
            Node *v = outEdges[i]->getEndNode();

            float alternative = dist[u] + outEdges[i]->getWeight();

            if (alternative < dist[v]) {
                float index = dist[v];
                dist[v] = alternative;
                prev[v] = u;

                q.erase(make_pair(v, index));
                q.insert(make_pair(v, dist[v]));
            }
        }
    }

    Node *u = end;

    while (prev[u] != nullptr) {
        result.insert(result.begin(), prev[u]->getEdge(u));
        u = prev[u];
    }

    return result;
}

/*
vector<Edge*> DiGraph::dijkstraShortestPath(Node *start, Node *end) {
    set<pair<Node*, float>> priorityQueue;
    vector<Edge*> result;
    float distance[nodes.size()];
    vector<Node*> previous;
    Node *u;

    int index = -1;

    for (int i = 0; i < nodes.size(); i++) {
        if (nodes[i] == start) {
            distance[i] = 0;
            index = i;
        } else distance[i] = INT_MAX;

        priorityQueue.insert(make_pair(nodes[i], distance[i]));
    }

    while (!priorityQueue.empty()) {
        u = priorityQueue.begin()->first;
        priorityQueue.erase(make_pair(u, priorityQueue.begin()->second));

        vector<Edge*> outEdges = u->getEdges();

        for (int i = 0; i < outEdges.size(); i++) {
            Node *v = outEdges[i]->getEndNode();
            float alternative = distance[index] + outEdges[i]->getWeight();

            if (alternative < distance[i]) {
                priorityQueue.erase(make_pair(v, distance[i]));

                distance[i] = alternative;
                previous.push_back(u);

                priorityQueue.insert(make_pair(v, alternative));
            }
        }
    }

    u = end;

    while (previous[index] != nullptr) {
        Edge *edge = new Edge();
        edge->setStartNode(previous[index]);
        edge->setEndNode(u);

        result.push_back(edge);
        u = previous[index];
    }

    return result;
}*/

void DiGraph::setVisualizer(GraphVisualizer *graphviz) {
    this->graphviz = graphviz;
}

GraphVisualizer* DiGraph::getVisualizer() {
    return graphviz;
}


#endif //OOA_PR4_DIGRAPH_H
