//
// Created by Kai Kuhlmann on 25.05.16.
//

#ifndef OOA_PR4_NAVIGATOR_H
#define OOA_PR4_NAVIGATOR_H

#include "DiGraph.h"
#include "../Vehicle/Vehicle.h"

using namespace std;

class Navigator {

private:
    DiGraph *graph;
    Vehicle *vehicle;

public:
    Navigator(DiGraph *graph);
    void setGraph(DiGraph *graph);
    void setVehicle(Vehicle *vehicle);
    float planRoute(Node *start, Node *end);
};

Navigator::Navigator(DiGraph *graph) {
    setGraph(graph);
}

void Navigator::setGraph(DiGraph *graph) {
    this->graph = graph;
}

void Navigator::setVehicle(Vehicle *vehilce) {
    this->vehicle = vehilce;
}

float Navigator::planRoute(Node *start, Node *end) {
    vector<Edge*> edges = graph->dijkstraShortestPath(start, end);

    float distance = 0;

    for (int i = 0; i < edges.size(); i++) {
        distance += edges[i]->getWeight();
    }

    graph->getVisualizer()->highlightPath(edges);

    return vehicle->neededTimeForDistance(distance);
}

#endif //OOA_PR4_NAVIGATOR_H
