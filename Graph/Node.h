//
// Created by Kai Kuhlmann on 04.05.16.
//

#ifndef OOA_PR4_NODE_H
#define OOA_PR4_NODE_H


#include <string>
#include <vector>
#include <list>
#include "Edge.h"

using namespace std;

class Node {

private:
    string node_key;
    int position_x, position_y;
    vector<Edge*> edges;

public:
    Node(string node_key, int position_x, int position_y);
    ~Node();

    string getKey();
    int getPositionX();
    int getPositionY();
    vector<Edge*> getEdges();
    Edge *getEdge(Node *node);

    void setKey(string key);
    void setPositionX(int x);
    void setPositionY(int y);
    void setNewEdge(Edge *edge);
};

Node::Node(string node_key, int position_x, int position_y) {
    this->node_key = node_key;
    this->position_x = position_x;
    this->position_y = position_y;
}

Node::~Node() {}

string Node::getKey() {
    return node_key;
}

int Node::getPositionX() {
    return position_x;
}

int Node::getPositionY() {
    return position_y;
}

vector<Edge*> Node::getEdges() {
    return edges;
}

Edge* Node::getEdge(Node *node) {
    vector<Edge*> edges = getEdges();

    for (int i = 0; i < edges.size(); i++) {
        if (edges[i]->getEndNode() == node) return edges[i];
    }

    return nullptr;
}

void Node::setKey(string key) {
    node_key = key;
}

void Node::setPositionX(int x) {
    position_x = x;
}

void Node::setPositionY(int y) {
    position_y = y;
}

void Node::setNewEdge(Edge *edge) {
    edges.push_back(edge);
}


#endif //OOA_PR4_NODE_H
