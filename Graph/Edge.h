//
// Created by Kai Kuhlmann on 04.05.16.
//

#ifndef OOA_PR4_EDGE_H
#define OOA_PR4_EDGE_H


class Node;

class Edge {

private:
    Node *start_node, *end_node;
    float weight;

public:
    Edge();
    ~Edge();

    float getWeight();
    Node* getStartNode();
    Node* getEndNode();

    void setWeight(float weight);
    void setStartNode(Node *node);
    void setEndNode(Node *node);
};

Edge::Edge() {}
Edge::~Edge() {}

float Edge::getWeight() {
    return weight;
}

Node* Edge::getStartNode() {
    return start_node;
}

Node* Edge::getEndNode() {
    return end_node;
}

void Edge::setWeight(float w) {
    weight = w;
}

void Edge::setStartNode(Node *node) {
    start_node = node;
}

void Edge::setEndNode(Node *node) {
    end_node = node;
}


#endif //OOA_PR4_EDGE_H
