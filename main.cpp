#include <iostream>
#include "Graph/DiGraph.h"
#include "Visualizer/OpenCVGraphVisualizer.h"
#include "Graph/Navigator.h"
#include "Vehicle/Car.h"

using namespace std;

void createDummyGraph(DiGraph &g) {
    g.addNode(new Node("Aachen", 100, 600));
    g.addNode(new Node("Berlin", 300, 650));
    g.addNode(new Node("Koeln", 300, 300));
    g.addNode(new Node("Essen", 900, 300));
    g.addNode(new Node("Bonn", 300, 150));
    g.addNode(new Node("Krefeld", 100, 160));

    g.addEdge("Aachen", "Berlin", 7);
    g.addEdge("Koeln", "Aachen", 9);
    g.addEdge("Aachen", "Krefeld", 7);
    g.addEdge("Berlin", "Essen", 40);
    g.addEdge("Berlin", "Koeln", 3);
    g.addEdge("Koeln", "Essen", 31);
    g.addEdge("Bonn", "Essen", 8);
    g.addEdge("Krefeld", "Bonn", 1);
}

int main() {
    DiGraph graph;
    graph.setVisualizer(new OpenCVGraphVisualizer());

    createDummyGraph(graph);

    graph.getVisualizer()->render(graph);

    Navigator navigator(&graph);
    navigator.setVehicle(new Car(120));

    cout << "Fahrzeit: " << navigator.planRoute(graph.getNode("Aachen"), graph.getNode("Essen")) << " Minuten" << endl;

    graph.getVisualizer()->show();

    return 0;
}